/**
 * Created by Raga Subekti
 */
import {Component, OnInit} from '@angular/core';
import {MdDialog, MdSnackBar, MdSnackBarConfig} from '@angular/material';
import {Router} from '@angular/router';
import {PostCancelModal} from '../../../modal/post/cancel/post.cancel.modal';
import {FormControl, Validators} from '@angular/forms';
import {PostService} from '../../../services/post.service';


@Component({
  selector: 'create-post',
  templateUrl: './post.create.component.html'
})
export class PostCreateComponent implements OnInit {
  subjectArray = [
    {checked: false, id: 'keluhan'},
    {checked: false, id: 'saran'},
    // {checked: false, id: 'pengumuman'},
  ];

  topicArray = [
    {checked: false, id: 'kesiswaan'},
    {checked: false, id: 'kurikulum'},
    {checked: false, id: 'sarana'},
    {checked: false, id: 'humas'}
  ];

  isEnabled = {
    image: true,
    video: true,
    document: true
  };

  postForm = {
    title: '',
    location: '',
    information: '',
    subject: [],
    topic: [],
    media: {
      type: '',
      files: []
    },
    public: true
  };

  formChecker = {
    title: new FormControl('', Validators.required),
    location: new FormControl('', Validators.required),
    information: new FormControl(''),
  };

  hasError = null;

  $formError = {on: null, error: ''};
  $mediaError = null;

  mediaThumbnail = [];

  titleFormControl = new FormControl('', Validators.required);

  isSendingPost = false;

  constructor(private dialog: MdDialog, private router: Router, private postService: PostService, private snackbar: MdSnackBar) {
  }

  ngOnInit() {
  }

  onFormSubmit() {
    this._convert();
    this._formValue();
    this.hasError = '';
    if (this._formCheckError()) {
      this.isSendingPost = true;
      setTimeout(() => {
        this.isSendingPost = false;
      }, 5000);
      this.postService.post(this.postForm)
        .then((result) => {
          this.snackbar.open('Kiriman berhasil terkirim!', '', <MdSnackBarConfig>{
            duration: 2500
          });
          // clear form on success

          this.postForm = {
            title: '',
            location: '',
            information: '',
            subject: [],
            topic: [],
            media: {
              type: '',
              files: []
            },
            public: true
          };

          this.isSendingPost = false;
          this.router.navigate(['/home']);
        }).catch((err) => {
        this.isSendingPost = false;
        this.hasError = err;
      })
    } else {
    }
  }


  _formValue() {
    const form = ['title', 'location', 'information'];
    for (let i = 0; i < form.length; i++) {
      this.postForm[form[i]] = this.formChecker[form[i]].value;
    }
  }

  onPostCancel() {
    this._convert();
    if (this._formCheckOnCancel()) {
      this.snackbar.open('Kiriman dibatalkan', '', <MdSnackBarConfig>{duration: 2500});
      this.router.navigate(['/home']);
    } else {
      this.dialog.open(PostCancelModal);
    }
  }


  _onImageChange($e) {
    // Disable other input
    this.isEnabled = {
      image: true,
      video: false,
      document: false
    };

    this.postForm.media.type = 'image';

    const files = $e.target.files;
    const pattern: RegExp = /image-*/;

    for (let i = 0; i < files.length; i++) {
      if (!files[i].type.match(pattern)) {
        this.$mediaError = 'Format file tidak sah!';

        this.isEnabled = {
          image: true,
          video: true,
          document: true
        };

        this.mediaThumbnail = [];
        return;
      } else {
        this.postForm.media.files.push(files[i]);

        const reader = new FileReader();
        reader.onload = (e) => {
          this.mediaThumbnail.push((<any> e.target).result);
        };

        reader.readAsDataURL(files[i]);
      }
    }

  }

  /**
   * Convert topic and subject object into array on postForm
   * @private
   */
  private _convert() {
    // first empty the array
    this.postForm.subject = [];
    this.postForm.topic = [];

    // then do the fucking looping
    for (let i = 0; i < this.subjectArray.length; i++) {
      if (this.subjectArray[i].checked) {
        this.postForm.subject.push(this.subjectArray[i].id);
      }
    }
    for (let i = 0; i < this.topicArray.length; i++) {
      if (this.topicArray[i].checked) {
        this.postForm.topic.push(this.topicArray[i].id);
      }
    }
  }

  private _formCheckError() {
    const form = this.postForm;
    this.hasError = null;

    const name = ['title', 'location', 'subject', 'topic'];
    const i18n = ['Judul', 'Lokasi', 'Subjek', 'Topik', 'Keterangan'];

    for (let i = 0; i < name.length; i++) {
      if (form[name[i]].length <= 0) {
        this.hasError = i18n[i] + ' wajib diisi/dipilih!';
        return false;
      }
    }

    return true;
  }

  private _formCheckOnCancel() {
    const form = this.postForm;

    const name = ['title', 'location', 'subject', 'topic', 'information'];

    for (let i = 0; i < name.length; i++) {
      if (form[name[i]].length > 0) {
        return false;
      }
    }

    return true;
  }
}
