import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {UsersService} from '../../../services/users.service';
import {MdSnackBar, MdSnackBarConfig} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'user-create',
  templateUrl: './create.user.component.html'
})

export class CreateUserComponent implements OnInit {

  public form;
  public $error: string;
  public userLevel = [
    {value: '0', text: 'Admin'},
    {value: '21', text: 'Kurikulum'},
    {value: '22', text: 'Kesiswaan'},
    {value: '23', text: 'Sarana'},
    {value: '24', text: 'Hubungan Masyarakat'},
    {value: '31', text: 'Guru'},
    {value: '32', text: 'Karyawan'},
    {value: '33', text: 'Siswa'},
    {value: '40', text: 'Umum'}
  ];


  constructor(private fb: FormBuilder, private userService: UsersService, private snackbar: MdSnackBar, private router: Router) {
    this.form = fb.group({
      username: ['', Validators.required],
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      password: ['', Validators.required],
      level: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  onFormSubmit() {
    this.$error = null;

    const form = ['username', 'name', 'email', 'phone', 'password'];
    const formValue = this.form.value;

    let Continue = false;

    for (let i = 0; i < form.length; i++) {
      if (formValue[form[i]].length <= 0) {
        Continue = false;
        return false;
      } else {
        Continue = true;
      }
    }

    if (Continue) {
      this.userService.createUser(formValue).then((result) => {
        this.router.navigate(['/user/list']);
        this.snackbar.open('Pengguna berhasil dibuat!', '', <MdSnackBarConfig>{duration: 3000})
      }).catch((err) => {
        this.$error = err;
      })
    }
  }
}
