import {Component, OnInit} from '@angular/core';

import {UsersService} from '../../../services/users.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user = {
    username: '',
    password: ''
  };
  public $error = null;
  public isLoading: Boolean = false;


  constructor(private userService: UsersService, private router: Router) {
  }

  ngOnInit() {
    if (this.userService.isLoggedIn && !localStorage.getItem('error')) {
      this.router.navigate(['/'])
    }
  }

  hasError() {
    return localStorage.getItem('error');
  }

  login() {
    this.isLoading = true;
    this.$error = null;
    this.userService.login(this.user.username, this.user.password)
      .then((result) => {
        this.isLoading = false;
        this.$error = null;
      }).catch((err) => {
      this.isLoading = false;
      this.$error = err;
    })
  }

}
