import {Component, OnInit} from '@angular/core';
import {UsersService} from '../../../services/users.service';

@Component({
    selector: 'app-user-logout',
    templateUrl: './user-logout.component.html',
    styleUrls: ['./user-logout.component.css']
})
export class UserLogoutComponent implements OnInit {

    constructor(private userService: UsersService) {
    }

    ngOnInit() {
        this.userService.logout();
    }

}
