import {Component, OnInit} from '@angular/core';
import {UsersService} from '../../../services/users.service';
import {MdDialog} from '@angular/material';
import {UserActivationModal} from '../../../modal/user/activation/user.activation.modal';
import {UserDeleteModal} from '../../../modal/user/delete/user.delete.modal';
import {UserUpdateModal} from '../../../modal/user/update/user.update.modal';

@Component({
  selector: 'app-user-list',
  templateUrl: './list.component.html'
})

export class UserListComponent implements OnInit {
  usersData: Array<any>;
  usersTotal: number;

  constructor(private userService: UsersService, private dialog: MdDialog) {
  }

  ngOnInit() {
    this._getData();
  }

  userLevel(level) {
    level = parseInt(level);
    if (level < 20) {
      return 'Admin';
    } else if (level === 21) {
      return 'Kurikulum';
    } else if (level === 22) {
      return 'Kesiswaan';
    } else if (level === 23) {
      return 'Sarana';
    } else if (level === 24) {
      return 'Humas';
    } else if (level === 31) {
      return 'Guru';
    } else if (level === 32) {
      return 'Karyawan';
    } else if (level === 33) {
      return 'Siswa';
    } else {
      return 'Umum';
    }
  }

  onUserUpdate(row) {
    const modal = this.dialog.open(UserUpdateModal);
    modal.componentInstance.data = row;

    modal.afterClosed().subscribe(() => {
      this._getData();
    })
  }

  onUserActivation() {
    const modal = this.dialog.open(UserActivationModal);
    modal.afterClosed().subscribe((result) => {
      this._getData();
    }, (err) => {
      console.log(err);
    })
  }

  onUserDelete(data) {
    const dialog = this.dialog.open(UserDeleteModal);
    dialog.componentInstance.data = data;
    dialog.afterClosed().subscribe(() => {
      this._getData();
    })
  }

  private _getData() {
    this.userService.getAll(0, 10000, null, null, null)
      .then((result) => {
        this.usersData = result['data'];
        this.usersTotal = result['total']
      }).catch((err) => {
      console.log(err)
    })
  }
}


