import {Component, OnInit} from '@angular/core';
import {PostService} from '../../services/post.service';
import {DeletePostModal} from '../../modal/post/delete/delete.post.modal';
import {NotifyService} from '../../services/notify.service';
import {MdDialog} from '@angular/material';
import {RespondModal} from '../../modal/post/respond/respond.modal';
import {ResponseModal} from '../../modal/post/response/response.modal';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  postArray = [];
  postTotal: number;

  isLoadingData: boolean;

  errorMessage: string;
  httpServer: string;

  constructor(private postService: PostService,
              private notifyService: NotifyService,
              private dialog: MdDialog) {

    if (localStorage.getItem('SERVER_API') !== null) {
      this.httpServer = localStorage.getItem('SERVER_API');
    } else {
      localStorage.setItem('SERVER_API', window.location.origin);
      this.httpServer = localStorage.getItem('SERVER_API');
    }
  }

  ngOnInit() {
    this._getPost();
  }

  genDate(date) {
    const o = new Date(date);
    const timeZoneFromDB = -7.00;
    const tzDifference = timeZoneFromDB * 60 + 0;
    return new Date(o.getTime() + tzDifference * 60 * 1000);
  }


  isHisPost(topic) {
    const user = [
      {id: 21, name: 'kurikulum'},
      {id: 22, name: 'kesiswaan'},
      {id: 23, name: 'sarana'},
      {id: 24, name: 'humas'}
    ];

    if (parseInt(localStorage.getItem('level')) < 20) {
      return true;
    } else {
      for (let i = 0; i < topic.length; i++) {
        for (let j = 0; j < 4; j++) {
          if (topic[i] == user[j]) {
            return true;
          }
        }
      }
    }
  }

  get userLevel() {
    if (localStorage.getItem('token')) {
      return parseInt(localStorage.getItem('level'));
    }
  }

  get username() {
    return localStorage.getItem('username');
  }

  convertImgUrl(url) {
    return localStorage.getItem('SERVER_API') + 'uploads/' + url;
  }


  // for media

  // post list
  onRespond(row) {
    const modal = this.dialog.open(RespondModal);
    modal.componentInstance.data = row;
    modal.afterClosed().subscribe(() => {
      this._getPost();
    }, () => {
      this._getPost();
    })
  }


  onViewResponse(row) {
    const modal = this.dialog.open(ResponseModal);
    modal.componentInstance.data = row;
    modal.afterClosed().subscribe(() => {
      this._getPost();
    }, () => {
      this._getPost();
    })
  }

  onPostDelete(row) {
    const modal = this.dialog.open(DeletePostModal);
    modal.componentInstance.data = row;
    modal.afterClosed().subscribe(() => {
      this._getPost();
    })
  }


  onPostUpdate(row) {
    // display update modal instead
    this.dialog.open(DeletePostModal);
  }

  onPostComment(row) {
  }

  // create new post
  private _getPost() {
    this.isLoadingData = true;

    this.postService.getAll(0, 1000)
      .then((result) => {

        this.isLoadingData = false;

        this.postArray = result['data'];
        this.postTotal = result['total'];

      })
      .catch((err) => {
        this.errorMessage = err;

        this.isLoadingData = false;
      })

  }
}
