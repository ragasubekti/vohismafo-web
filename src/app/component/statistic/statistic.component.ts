/**
 * Created by Raga Subekti
 */
import {Component, OnInit} from '@angular/core';
import {StatisticService} from '../../services/statistic.service';
import {Chart} from 'angular-highcharts';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html'
})

export class StatisticComponent implements OnInit {
  barChart: any;
  categories = [];
  dataset = {
    selesai: [],
    konfirmasi: [],
    belum: []
  };
  isLoading = false;
  chartType = 'bar';
  error: string;

  constructor(private statService: StatisticService) {
    this._initializeChart('daily');
    this.getDailyChart();
  }

  ngOnInit() {
    this._initializeChart('monthly');
    this.getMonthlyChart()
  }

  onPieClicked() {
    this.chartType = 'pie';
  }

  onDailyClicked() {
    this._initializeChart('daily');
    this.getDailyChart();
  }

  onMonthlyClicked() {
    this._initializeChart('monthly');
    this.getMonthlyChart();
  }

  onYearlyClicked() {
    this._initializeChart('yearly');
    this.getYearlyChart();
  }

  getYearlyChart() {
    // this.isLoading = true;
    // this.statService.yearlyStatitistic()
    //     .then((result) => {
    //     for(let i = 0; i < this.categories.length; i++) {
    //         this.dataset.keluhan[i] = parseInt(result['keluhan'][this.categories[i]]);
    //         this.dataset.saran[i] = parseInt(result['saran'][this.categories[i]]);
    //         this.dataset.pengumuman[i] = parseInt(result['pengumuman'][this.categories[i]]);
    //     }
    //         // for (result['keluhan'] in obj) {
    //         //     this.dataset.keluhan[result['keluhan'][i].date - 1] = parseInt(result['keluhan'][i].total);
    //         // }
    //         //
    //         // for (let i = 0; i < result['saran'].length; i++) {
    //         //     this.dataset.saran[result['saran'][i].date - 1] = parseInt(result['saran'][i].total);
    //         // }
    //         //
    //         // for (let i = 0; i < result['pengumuman'].length; i++) {
    //         //     this.dataset.pengumuman[result['pengumuman'][i].date - 1] = parseInt(result['pengumuman'][i].total);
    //         // }
    //
    //         this.isLoading = false;
    //         setTimeout(() => this._generateChart('yearly'), 100);
    //     })
  }

  private _generateChart(type) {
    const title = {daily: 'Harian', monthly: 'Bulanan', yearly: 'Tahunan'};
    if (this.chartType == 'bar') {
      this.barChart = new Chart({
        chart: {
          type: 'column'
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Jumlah'
          }
        },
        xAxis: {
          categories: this.categories,
          crosshair: true
        },
        title: {text: 'Statistik Laporan'},
        series: [{
          name: 'Belum Ditanggapi',
          data: this.dataset.belum
        }, {
          name: 'Konfirmasi',
          data: this.dataset.konfirmasi
        }, {
          name: 'Selesai',
          data: this.dataset.selesai
        }]
      });
    } else if (this.chartType == 'pie') {

    }
  }

  private getMonthlyChart() {
    this.isLoading = true;
    this.statService.monthlyStatistic()
      .then((result) => {
        const month = new Date().getMonth() + 1;
        for (let i = 0; i < result['kesiswaan'].length; i++) {
          if (parseInt(result['kesiswaan'][i].date) == month) {
            const current = result['kesiswaan'][i];
            console.log(current)
            if (current.type == null) {
              this.dataset.belum[0] = parseInt(current.total)
            } else if (current.type == 'konfirmasi') {
              this.dataset.konfirmasi[0] = parseInt(current.total)
            } else if (current.type == 'selesai') {
              this.dataset.selesai[0] = parseInt(current.total)
            }
          }
        }

        for (let i = 0; i < result['kurikulum'].length; i++) {
          if (parseInt(result['kurikulum'][i].date) == month) {
            const current = result['kurikulum'][i];
            console.log(current)
            if (current.type == null) {
              this.dataset.belum[1] = parseInt(current.total)
            } else if (current.type == 'konfirmasi') {
              this.dataset.konfirmasi[1] = parseInt(current.total)
            } else if (current.type == 'selesai') {
              this.dataset.selesai[1] = parseInt(current.total)
            }
          }
        }

        for (let i = 0; i < result['sarana'].length; i++) {
          if (parseInt(result['sarana'][i].date) == month) {
            const current = result['sarana'][i];
            console.log(current)
            if (current.type == null) {
              this.dataset.belum[2] = parseInt(current.total)
            } else if (current.type == 'konfirmasi') {
              this.dataset.konfirmasi[2] = parseInt(current.total)
            } else if (current.type == 'selesai') {
              this.dataset.selesai[2] = parseInt(current.total)
            }
          }
        }

        for (let i = 0; i < result['humas'].length; i++) {
          if (parseInt(result['humas'][i].date) == month) {
            const current = result['humas'][i];
            console.log(current)
            if (current.type == null) {
              this.dataset.belum[3] = parseInt(current.total)
            } else if (current.type == 'konfirmasi') {
              this.dataset.konfirmasi[3] = parseInt(current.total)
            } else if (current.type == 'selesai') {
              this.dataset.selesai[3] = parseInt(current.total)
            }
          }
        }

        this.isLoading = false;
        setTimeout(() => this._generateChart('monthly'), 100);
      }).catch(err => {
      this.isLoading = false;
      this.error = err;
    })
  }

  private getDailyChart() {
    // this.isLoading = true;
    // this.statService.dailyStatistic()
    //     .then((result) => {
    //         for (let i = 0; i < result['keluhan'].length; i++) {
    //             this.dataset.keluhan[result['keluhan'][i].date - 1] = parseInt(result['keluhan'][i].total);
    //         }
    //
    //         for (let i = 0; i < result['saran'].length; i++) {
    //             this.dataset.saran[result['saran'][i].date - 1] = parseInt(result['saran'][i].total);
    //         }
    //
    //         for (let i = 0; i < result['pengumuman'].length; i++) {
    //             this.dataset.pengumuman[result['pengumuman'][i].date - 1] = parseInt(result['pengumuman'][i].total);
    //         }
    //
    //         this.isLoading = false;
    //         setTimeout(() => this._generateChart('daily'), 100);
    //     })
  }

  /**
   * Initialize the component of statistic
   * @param type (daily, monthly, yearly)
   * @private
   */
  private _initializeChart(type): void {
    const date = new Date();

    this.categories = [];
    this.dataset = {
      selesai: [],
      konfirmasi: [],
      belum: []
    };

    if (type == 'daily') {
      for (let i = 1; i <= date.getDate(); i++) {
        // this.categories.push(i);
        // this.dataset.keluhan.push(0);
        // this.dataset.saran.push(0);
        // this.dataset.pengumuman.push(0);
      }
    } else if (type == 'monthly') {
      const months = ['Kesiswaan', 'Kurikulum', 'Sarana & Prasarana', 'Hubungan Masyarakat'];
      for (let i = 0; i < 4; i++) {
        this.categories.push(months[i]);
        this.dataset.selesai.push(0);
        this.dataset.konfirmasi.push(0);
        this.dataset.belum.push(0);
      }
    } else if (type == 'yearly') {
      for (let i = 4; i >= 0; i--) {
        // this.categories.push(date.getFullYear() - i);
        // this.dataset.keluhan.push(0);
        // this.dataset.saran.push(0);
        // this.dataset.pengumuman.push(0);
      }
    }
  }


}
