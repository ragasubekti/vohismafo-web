/**
 * Created by Raga Subekti
 */

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PostService} from '../../services/post.service';
import {MdDialog, MdSnackBar, MdSnackBarConfig} from '@angular/material';
import {RespondModal} from '../../modal/post/respond/respond.modal';
import {ResponseModal} from '../../modal/post/response/response.modal';
import {CommentService} from '../../services/comment.service';
import {CommentDeleteModal} from '../../modal/comment/delete/comment.delete.modal';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html'
})

export class CommentComponent implements OnInit {
  id: number;
  post: Object;
  $error: string;
  isLoading: boolean;
  commentsArray = [];
  comments = '';
  commentError = null;
  isPostSending = false;

  constructor(private route: ActivatedRoute, private postService: PostService, private dialog: MdDialog, private commentService: CommentService, private snackbar: MdSnackBar) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this._getPost(this.id);
    });
  }

  onCommentSubmit() {
    this.isPostSending = true;
    this.commentError = null;


    if (this.comments.length <= 0) {
      this.commentError = 'Mohon isi komentar!';
    } else {

      setTimeout(() => {
        this.isPostSending = false;
      }, 5000);

      this.commentService.postComment(this.id, this.comments)
        .then(result => {
          this.isPostSending = false;
          this.comments = '';
          this.snackbar.open('Komentar berhasil terkirim!', '', <MdSnackBarConfig>{duration: 3000});
          this._getComment();
        })
        .catch(err => {
          this.isPostSending = false;
          this.commentError = err;
        })
    }
  }

  get username() {
    return localStorage.getItem('username');
  }

  get userLevel() {
    if (localStorage.getItem('token')) {
      return parseInt(localStorage.getItem('level'));
    }
  }

  onCommentDelete(row) {
    const modal = this.dialog.open(CommentDeleteModal);
    modal.componentInstance.data = row;
    modal.afterClosed().subscribe(() => {
      this._getComment();
    })
  }

  userInfo(type) {
    return localStorage.getItem(type);
  }

  convertImgUrl(url) {
    return localStorage.getItem('SERVER_API') + '/api/uploads/' + url;
  }

  onRespond(row) {
    const modal = this.dialog.open(RespondModal);
    modal.componentInstance.data = row;
    modal.afterClosed().subscribe(() => {
      this._getPost(this.id);
    }, () => {
      this._getPost(this.id);
    })
  }


  onViewResponse(row) {
    const modal = this.dialog.open(ResponseModal);
    modal.componentInstance.data = row;
  }

  private _getComment() {
    this.commentService.getComment(this.id)
      .then(comments => {
        this.commentsArray = comments['data'];
      })
      .catch(err => {
        this.$error = err;
      })
  }

  private _getPost(id) {
    this.isLoading = true;
    this.postService.get(id)
      .then((result) => {
        this.post = result;
        this.commentService.getComment(id)
          .then(comments => {
            this.isLoading = false;
            this.commentsArray = comments['data'];
          })
          .catch(err => {
            this.isLoading = false;
            this.$error = err;
          })
        // this.isLoading = false;
      }).catch((err) => {
      this.isLoading = false;
      this.$error = err;
    })
  }
}
