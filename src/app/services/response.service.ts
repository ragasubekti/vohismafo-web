/**
 * Created by Raga Subekti
 */
import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';

@Injectable()
export class ResponseService {

  constructor(private http: Http) {
  }

  getResponse(id) {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const params = new URLSearchParams();
      params.append('id', id);

      const option = new RequestOptions({headers: headers, params: params});

      const url = this.server('response');
      this.http.get(url, option).subscribe(
        (result) => {
          if (result.json()['error']) {
            return reject(result.json()['error']);
          } else {
            return resolve(result.json());
          }
        }, (err) => {
          return reject(err.statusText);
        }
      )
    })
  }

  setResponse(id, response, information) {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const option = new RequestOptions({headers: headers});

      const url = this.server('response');
      this.http.post(url, {id: id, type: response, information: information}, option).subscribe(
        (result) => {
          if (result.json()['error']) {
            return reject(result.json()['error']);
          } else {
            return resolve(result.json());
          }
        }, (err) => {
          return reject(err.statusText);
        }
      )
    });
  }

  private server(page) {
    if (!localStorage.getItem('SERVER_API')) {
      localStorage.setItem('SERVER_API', 'http://vohismareporter.ml/api/');
      return localStorage.getItem('SERVER_API') + page;
    } else {
      return localStorage.getItem('SERVER_API') + page;
    }
  }
}
