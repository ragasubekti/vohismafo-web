import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Router} from '@angular/router';


@Injectable()
export class UsersService {

  loggedIn: boolean;

  constructor(private http: Http, private router: Router) {
    /**
     * Set user login status
     */
    this.loggedIn = !!localStorage.getItem('token');
  }

  get isLoggedIn() {
    return this.loggedIn;
  }

  /**
   * Login service
   * @param username
   * @param password
   * @returns {Promise<any>}
   */
  login(username, password) {
    return new Promise((resolve, reject) => {
      this.http.post(this.server('authentication'), {
        username: username,
        password: password
      }).subscribe((result) => {
        result = result.json();
        if (result['token']) {
          // if (parseInt(result['user_level']) > 30) {
          //   return reject('Gunakan aplikasi VOHISMA REPORTER untuk mengakses!');
          // } else {
          if (localStorage.getItem('error')) {
            localStorage.removeItem('error');
          }

          this.loggedIn = !!localStorage.getItem('token');
          const user = result['data'];

          localStorage.setItem('token', result['token']);
          localStorage.setItem('username', user.username);
          localStorage.setItem('name', user.full_name);
          localStorage.setItem('level', user.user_level);
          // this.router.navigate(['/home']);
          location.href = '/home';

          return resolve(result);
          // }
        } else {
          return reject(result['error']);
        }
      }, (err) => {
        return reject(err.statusText);
      })
    });
  }

  /**
   * Pretty obvious
   */
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    localStorage.removeItem('name');
    localStorage.removeItem('level');

    this.loggedIn = !!localStorage.getItem('token');
    // setTimeout(() => {
    // this.router.navigate(['/user/login']);
    location.href = '/user/login';
    return true;
    // }, 1000);
  };

  /**
   * I don't think this thing will be used in the future lol
   * @param user
   * @returns {Promise<any>}
   */
  createUser(user) {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const options = new RequestOptions({headers: headers});

      this.http.post(this.server('user'), {
        username: user.username,
        password: user.password,
        name: user.name,
        email: user.email,
        phone: user.phone,
        level: user.level
      }, options).subscribe((result) => {
        result = result.json();
        if (result['error']) {
          return reject(result['error']);
        } else {
          return resolve(result);
        }
      }, (err) => {
        return reject(err.statusText);
      })
    })
  }

  confirmUser(id) {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));
      const options = new RequestOptions({headers: headers});

      this.http.put(this.server('user'), {
        id: id
      }, options).subscribe((result) => {
        result = result.json();
        if (result['error']) {
          return reject(result);
        } else {
          return resolve(result);
        }
      }, (err) => {
        return reject(err);
      })
    })
  }

  deleteUser(id) {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const options = new RequestOptions({headers: headers});

      this.http.delete(this.server('user/' + id), options).subscribe((result) => {
        result = result.json();
        if (result['error']) {
          return reject(result);
        } else {
          return resolve(result);
        }
      }, (err) => {
        return reject(err);
      })
    })
  }

  getAll(start, count, filter, sort, query) {
    return new Promise((resolve, reject) => {
      const url = this.server('user');

      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const params = new URLSearchParams();
      params.append('start', start);
      params.append('count', count);
      params.append('filter', filter);
      params.append('sort', sort);
      params.append('query', query);

      const options = new RequestOptions({headers: headers, params: params});

      this.http.get(url, options).subscribe((result) => {

        if (result.json()['error']) {
          localStorage.setItem('error', result.json()['error']);
          return reject(result.json());
        } else {
          return resolve(result.json());
        }
      }, (err) => {
        localStorage.setItem('error', err.json()['error']);
        return reject(err.json());
      })
    })
  }

  private server(page) {
    if (!localStorage.getItem('SERVER_API')) {
      localStorage.setItem('SERVER_API', 'http://vohismareporter.ml/api/');
      return localStorage.getItem('SERVER_API') + page;
    } else {
      return localStorage.getItem('SERVER_API') + page;
    }
  }
}
