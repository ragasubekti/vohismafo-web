import {Injectable} from '@angular/core';
import * as Noty from 'noty';

@Injectable()
export class NotifyService {

    constructor() {
    }

    success(message) {
        new Noty({
            type: 'success',
            layout: 'topRight',
            theme: 'bootstrap-v4',
            timeout: 5000,
            text: `<i class="fa fa-check"></i> ${message}`
        }).show()
    }

    error(message) {
        new Noty({
            type: 'error',
            layout: 'topRight',
            theme: 'bootstrap-v4',
            text: `<i class="fa fa-times"></i> ${message}`
        }).show()
    }

}
