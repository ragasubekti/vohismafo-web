import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';

@Injectable()
export class PostService {

  constructor(private http: Http) {
  }

  get(id) {
    return new Promise((resolve, reject) => {
      const url = this.server('get-post');

      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const params = new URLSearchParams();
      params.append('id', id);

      const option = new RequestOptions({headers: headers, params: params});

      this.http.get(url, option).subscribe(
        (result) => {
          if (result.json()['error']) {
            return reject(result.json()['error']);
          } else {
            return resolve(result.json());
          }
        }, (err) => {
          return reject(err.statusText);
        }
      );
    })
  }

  getAll(start, count) {
    return new Promise((resolve, reject) => {
      const url = this.server('post');

      const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

        const params = new URLSearchParams();
        params.append('start', start);
        params.append('counts', '100');

        const option = new RequestOptions({headers: headers, params: params});

        this.http.get(url, option).subscribe(
          result => {
            if (result.json()['error']) {
              return reject(result.json()['error']);
            } else {
              return resolve(result.json());
            }
          }, err => {
            return reject(err.statusText);
          }
        )
      }
    )
  }

  post(data) {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const option = new RequestOptions({headers: headers});

      if (data.media.files.length <= 0) {
        this.http.post(this.server('post'), {
          title: data.title,
          location: data.location,
          information: data.information,
          subject: data.subject,
          topic: data.topic,
          public: data.public,
          media: {
            type: '',
            files: []
          }
        }, option).subscribe(result => {
          if (result.json()['error']) {
            return reject(result.json()['error']);
          } else {
            return resolve(result.json());
          }
        }, err => {
          return reject(err.statusText);
        })
      } else {
        this._uploadMedia(data.media.files)
          .then((uplRes) => {
            this.http.post(this.server('post'), {
              title: data.title,
              location: data.location,
              information: data.information,
              subject: data.subject,
              topic: data.topic,
              media: {
                type: data.media.type,
                files: uplRes
              },
              public: data.public
            }, option).subscribe(result => {
              if (result.json()['error']) {
                return reject(result.json()['error']);
              } else {
                return resolve(result.json());
              }
            }, err => {
              return reject(err.statusText);
            })
          })
          .catch((err) => {
            return reject(err);
          });
      }
    });
  }

  update(id, status) {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const option = new RequestOptions({headers: headers});

      this.http.put(this.server('status'), {
        id: id,
        status: status
      }, option).subscribe(
        result => {
          if (result.json()['error']) {
            return reject(result.json()['error']);
          } else {
            return resolve(result.json());
          }
        }, err => {
          return reject(err.statusText);
        }
      )
    })
  }

  delete(id) {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const option = new RequestOptions({headers: headers});
      this.http.delete(this.server('post/' + id), option).subscribe(
        result => {
          if (result.json()['error']) {
            return reject(result.json()['error']);
          } else {
            return resolve(result.json());
          }
        }, err => {
          return reject(err.json())
        }
      )
    })

  }

  private _uploadMedia(array) {
    return new Promise((resolve, reject) => {
      const header = new Headers();
      header.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const option = new RequestOptions({headers: header});

      const formData = new FormData();
      if (array.length > 0) {
        for (let i = 0; i < array.length; i++) {
          formData.append('webfile[]', array[i]);
        }
        this.http.post(this.server('upload'), formData, option)
          .subscribe((result) => {
            if (result.json()['error']) {
              return reject(result.json()['error']);
            } else {
              return resolve(result.json());
            }
          }, (err) => {
            return reject(err.statusText);
          })
      }
    });
  }

  private server(page) {
    if (!localStorage.getItem('SERVER_API')) {
      localStorage.setItem('SERVER_API', 'http://vohismareporter.ml/api/');
      return localStorage.getItem('SERVER_API') + page;
    } else {
      return localStorage.getItem('SERVER_API') + page;
    }
  }


}
