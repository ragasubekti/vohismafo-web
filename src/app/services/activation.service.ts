import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';

@Injectable()
export class ActivationService {

  constructor(private http: Http) {
  }

  /**
   * Service to get list of user
   * @param query
   */
  autocomplete(query) {
    return new Promise((resolve, reject) => {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

        const params = new URLSearchParams();
        params.append('query', query);

        const option = new RequestOptions({headers: headers, params: params});

        this.http.get(this.server('activation'), option).subscribe(
          result => {
            if (result.json()['error']) {
              return reject(result.json()['error']);
            } else {
              return resolve(result.json());
            }
          }, err => {
            return reject(err.json()['error']);
          }
        )
      }
    )
  }

  /**
   * Service for activating user
   * @param username
   * @param code
   * @returns {Promise<any>}
   */
  activate(username, code) {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const option = new RequestOptions({headers: headers});

      this.http.put(this.server('activation'), {username: username, code: code}, option).subscribe(
        (result) => {
          if (result.json()['error']) {
            return reject(result.json()['error']);
          } else {
            return resolve(result.json());
          }
        }, (err) => {
          return reject(err.statusText);
        }
      );
    })
  }

  private server(page) {
    if (!localStorage.getItem('SERVER_API')) {
      localStorage.setItem('SERVER_API', 'http://vohismareporter.ml/api/');
      return localStorage.getItem('SERVER_API') + page;
    } else {
      return localStorage.getItem('SERVER_API') + page;
    }
  }

}
