/**
 * Created by Raga Subekti
 */
import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';

@Injectable()
export class StatisticService {

    constructor(private http: Http) {
    }

    // dailyStatistic() {
    //     return new Promise((resolve, reject) => {
    //         const headers = new Headers();
    //         headers.append('Content-Type', 'application/json');
    //         headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));
    //
    //         const params = new URLSearchParams();
    //         params.append('filter', 'daily');
    //
    //         const options = new RequestOptions({headers: headers, params: params});
    //
    //         this.http.get(this.httpServer + '/api/statistic', options).subscribe((result) => {
    //             if (result.json()['error']) {
    //                 return reject(result.json());
    //             } else {
    //                 return resolve(result.json());
    //             }
    //         }, (err) => {
    //             return reject(err.statusText);
    //         })
    //     })
    // }

    monthlyStatistic() {
        return new Promise((resolve, reject) => {
            const headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

            const params = new URLSearchParams();
            params.append('filter', 'monthly');
            params.append('type', 'topic');

            const options = new RequestOptions({headers: headers, params: params});

            this.http.get(this.server('statistic'), options).subscribe((result) => {
                if (result.json()['error']) {
                    return reject(result.json());
                } else {
                    return resolve(result.json());
                }
            }, (err) => {
                return reject(err.statusText);
            })
        })
    }

    // yearlyStatitistic() {
    //     return new Promise((resolve, reject) => {
    //         const headers = new Headers();
    //         headers.append('Content-Type', 'application/json');
    //         headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));
    //
    //         const params = new URLSearchParams();
    //         params.append('filter', 'yearly');
    //
    //         const options = new RequestOptions({headers: headers, params: params});
    //
    //         this.http.get(this.httpServer + '/api/statistic', options).subscribe((result) => {
    //             if (result.json()['error']) {
    //                 return reject(result.json());
    //             } else {
    //                 return resolve(result.json());
    //             }
    //         }, (err) => {
    //             return reject(err.statusText);
    //         })
    //     })
    // }


  private server(page) {
    if (!localStorage.getItem('SERVER_API')) {
      localStorage.setItem('SERVER_API', 'http://vohismareporter.ml/api/');
      return localStorage.getItem('SERVER_API') + page;
    } else {
      return localStorage.getItem('SERVER_API') + page;
    }
  }
}
