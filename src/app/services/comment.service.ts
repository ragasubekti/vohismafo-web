/**
 * Created by Raga Subekti
 */
import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';

@Injectable()
export class CommentService {

  constructor(private http: Http) {
  }

  getComment(id) {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const params = new URLSearchParams();
      params.append('id', id);

      const option = new RequestOptions({headers: headers, params: params});

      const url = this.server('comment');
      this.http.get(url, option).subscribe(
        (result) => {
          if (result.json()['error']) {
            return reject(result.json()['error']);
          } else {
            return resolve(result.json());
          }
        }, (err) => {
          return reject(err.statusText);
        }
      )
    })
  }

  deleteComment(id) {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));


      const option = new RequestOptions({headers: headers});

      const url = this.server('comment/' + id);
      this.http.delete(url, option).subscribe(
        (result) => {
          if (result.json()['error']) {
            return reject(result.json()['error']);
          } else {
            return resolve(result.json());
          }
        }, (err) => {
          return reject(err.statusText);
        }
      )
    })
  }


  postComment(id, comment) {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const option = new RequestOptions({headers: headers});

      const url = this.server('comment');

      this.http.post(url, {
        id: id,
        comment: comment
      }, option).subscribe(result => {
        if (result.json()['error']) {
          return reject(result.json()['error']);
        } else {
          return resolve(result.json())
        }
      }, err => {
        return reject(err.statusText);
      })
    })
  }

  private server(page) {
    if (!localStorage.getItem('SERVER_API')) {
      localStorage.setItem('SERVER_API', 'http://vohismareporter.ml/api/');
      return localStorage.getItem('SERVER_API') + page;
    } else {
      return localStorage.getItem('SERVER_API') + page;
    }
  }

}
