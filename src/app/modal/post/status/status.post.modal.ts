/**
 * Created by ragasub on 8/1/17.
 */
import {Component, Input} from '@angular/core';
// import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {PostService} from '../../../services/post.service';
import {NotifyService} from '../../../services/notify.service';

// import {isUndefined} from 'util';

@Component({
  selector: 'post-status-modal',
  templateUrl: './status.post.modal.html'
})
export class StatusPostModal {
  @Input() data;

  selectedStatus: string;
  statusError: string;

  constructor(private postService: PostService,
              private notifyService: NotifyService) {
  }

  onStatusDeny() {
    this.selectedStatus = 'tolak';
  }

  onStatusConfirm() {
    this.selectedStatus = 'konfirmasi';
  }

  onStatusProcess() {
    this.selectedStatus = 'proses';
  }

  onStatusDone() {
    this.selectedStatus = 'selesai';
  }

  onStatusSave() {
    // if (!this.selectedStatus || isUndefined(this.selectedStatus) || this.selectedStatus.length <= 0) {
    //   this.statusError = 'Status belum terpilih atau sama';
    // } else if (this.selectedStatus === this.data.status) {
    //   this.statusError = 'Status sama dengan sebelumnya';
    // } else {
    //   const id = this.data.id;
    //   this.postService.updateStatus(id, this.selectedStatus)
    //     .then(() => {
    //       this.activeModal.close();
    //       this.notifyService.success('Status sukses diubah');
    //     })
    //     .catch((err) => {
    //       this.statusError = err;
    //     })
    // }
  }

}
