/**
 * Created by ragasub on 8/1/17.
 */
import {Component, Input} from '@angular/core';
import {PostService} from '../../../services/post.service';
import {MdDialogRef, MdSnackBar, MdSnackBarConfig} from '@angular/material';

@Component({
  selector: 'post-delete-modal',
  templateUrl: './delete.post.modal.html'
})
export class DeletePostModal {
  @Input() data;

  constructor(private postService: PostService, private snackbar: MdSnackBar, private current: MdDialogRef<DeletePostModal>) {
  }


  onPostConfirm(data) {
    this.postService.delete(data.id)
      .then((result) => {
        this.snackbar.open('Kiriman berhasil dihapus', '', <MdSnackBarConfig>{
          duration: 3000
        });
        this.current.close();
      }).catch((err) => {
      console.log(err)
    })
  }
}
