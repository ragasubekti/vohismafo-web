/**
 * Created by Raga Subekti
 */
import {Component} from '@angular/core';
import {ResponseService} from '../../../services/response.service';
import {MdDialogRef, MdSnackBar, MdSnackBarConfig} from '@angular/material';

@Component({
  selector: 'post-respond-modal',
  templateUrl: './respond.modal.html'
})

export class RespondModal {
  public data;
  statusArray = [
    {id: 'konfirmasi', text: 'KONFIRMASI'},
    {id: 'selesai', text: 'SELESAI'}
  ];

  postData;
  postForm = {
    response: '',
    comment: ''
  };

  isLoading = true;

  constructor(private response: ResponseService, private snackbar: MdSnackBar, private current: MdDialogRef<RespondModal>) {
    setTimeout(() => {
      this.response.getResponse(this.data.id)
        .then((result) => {
          this.isLoading = false
          this.postData = result;
        }).catch((err) => {
        console.log(err)
      })
    });
  }

  onPostConfirm() {
    const id = this.data.id;
    const response = this.postForm.response;
    const comment = this.postForm.comment;
    this.response.setResponse(id, response, comment)
      .then((result) => {
        this.snackbar.open('Tanggapan berhasil disimpan!', '', <MdSnackBarConfig>{
          duration: 3000
        });
        this.current.close();
      }).catch(err => {
      console.log(err);
    })
  }


}
