/**
 * Created by Raga Subekti
 */
import {Component} from '@angular/core';
import {ResponseService} from '../../../services/response.service';
import {MdDialogRef, MdSnackBar} from '@angular/material';

@Component({
  selector: 'post-response-modal',
  templateUrl: './response.modal.html'
})

export class ResponseModal {
  public data;

  postData;

  isLoading: boolean;

  constructor(private response: ResponseService, private snackbar: MdSnackBar, private current: MdDialogRef<ResponseModal>) {
    setTimeout(() => {
      this.isLoading = true;
      this.response.getResponse(this.data.id)
        .then((result) => {
          this.isLoading = false;
          this.postData = result;
        }).catch((err) => {
        this.isLoading = false;
        console.log(err)
      })
    });
  }


}
