import {Component} from '@angular/core';
import {MdDialogRef, MdSnackBar} from '@angular/material';
import {UsersService} from '../../../services/users.service';

@Component({
  selector: 'user-delete-modal',
  templateUrl: './user.delete.modal.html',
})

export class UserDeleteModal {
  public data;
  isLoading = false;
  hasError = null;

  constructor(private thisDialog: MdDialogRef<UserDeleteModal>, private userService: UsersService, private snackbar: MdSnackBar) {}

  onDeleteConfirm() {
    this.isLoading = true;
    this.hasError = null;
    this.userService.deleteUser(this.data.user_id)
      .then(result => {
        this.isLoading = false;
        this.thisDialog.close();
        this.snackbar.open('Pengguna berhasil dihapus!', '' , {
          duration: 3000
        })
      })
      .catch(err => {
        this.isLoading = false;
        this.hasError = err;
      })
  }
}
