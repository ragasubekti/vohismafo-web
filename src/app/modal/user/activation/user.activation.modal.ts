/**
 * Created by Raga Subekti
 */
import {Component} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ActivationService} from '../../../services/activation.service';
import {MdDialogRef, MdSnackBar, MdSnackBarConfig} from '@angular/material';

@Component({
  selector: 'user-activation-modal',
  templateUrl: './user.activation.modal.html',
})

export class UserActivationModal {
  usernameArray = [];
  username = new FormControl();
  code = new FormControl();

  $error: string;

  constructor(private activationService: ActivationService, private current: MdDialogRef<UserActivationModal>, private snackbar: MdSnackBar) {
    this._getData('');
  }

  onFilter() {
    let query = this.username.value;
    this.usernameArray = [];
    this._getData(query);
  }

  onCodeChange() {
    this.code.patchValue(this.code.value.toUpperCase());
  }

  onActivate() {
    const username = this.username.value;
    const code = this.code.value;

    this.activationService.activate(username, code)
      .then((result) => {
        this.snackbar.open('Pengguna sukses diaktifkan', '', <MdSnackBarConfig>{
          duration: 3000
        });
        this.current.close();
      }).catch((err) => {
      this.$error = err;
    })
  }

  private _getData(query) {
    this.activationService.autocomplete(query).then((result) => {
      this.usernameArray = result['data'];
    }).catch((err) => {
      console.log(err);
    })
  }
}
