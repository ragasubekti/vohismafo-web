import {Component} from '@angular/core';
import {MdDialogRef, MdSnackBar} from '@angular/material';
import {UsersService} from '../../../services/users.service';

@Component({
  selector: 'user-update-modal',
  templateUrl: './user.update.modal.html',
})

export class UserUpdateModal {
  public data;
  isLoading = false;
  hasError = null;
  public userLevel = [
    {value: '0', text: 'Admin'},
    {value: '21', text: 'Kurikulum'},
    {value: '22', text: 'Kesiswaan'},
    {value: '23', text: 'Sarana'},
    {value: '24', text: 'Hubungan Masyarakat'},
    {value: '31', text: 'Guru'},
    {value: '32', text: 'Karyawan'},
    {value: '33', text: 'Siswa'},
    {value: '40', text: 'Umum'}
  ];

  constructor(private thisDialog: MdDialogRef<UserUpdateModal>, private userService: UsersService, private snackbar: MdSnackBar) {}

  onUpdateSubmit() {

  }

}
