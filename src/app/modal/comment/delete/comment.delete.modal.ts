/**
 * Created by ragasub on 8/1/17.
 */
import {Component, Input} from '@angular/core';
import {MdDialogRef, MdSnackBar, MdSnackBarConfig} from '@angular/material';
import {CommentService} from '../../../services/comment.service';

@Component({
  selector: 'comment-delete-modal',
  templateUrl: './comment.delete.modal.html'
})
export class CommentDeleteModal {
  @Input() data;

  constructor(private commentService: CommentService, private snackbar: MdSnackBar, private current: MdDialogRef<CommentDeleteModal>) {
  }


  onPostConfirm(data) {
    this.commentService.deleteComment(data.id)
      .then((result) => {
        this.snackbar.open('Komentar berhasil dihapus', '', <MdSnackBarConfig>{
          duration: 3000
        });
        this.current.close();
      })
      .catch((err) => {
        console.log(err);
        this.current.close();
      })
  }
}
