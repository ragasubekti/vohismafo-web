/**
 * Created by ragasub on 7/23/17.
 */
import {RouterModule, Routes} from '@angular/router';

import {AuthGuard} from './auth.guard';

import {LoginComponent} from './component/user/login/login.component';
import {HomeComponent} from './component/home/home.component';
import {UserLogoutComponent} from './component/user/logout/user-logout.component';
import {UserListComponent} from './component/user/list/list.component';
import {PostCreateComponent} from './component/post/create/post.create.component';
import {CreateUserComponent} from './component/user/create/create.user.component';
import {StatisticComponent} from './component/statistic/statistic.component';
import {ReportComponent} from './component/report/report.component';
import {CommentComponent} from './component/comment/comment.component';
import {AdminGuard} from './admin.guard';
import {TeacherGuard} from './teacher.guard';
import {LandingComponent} from "./landing/landing.component";

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'post',
    children: [
      {
        path: 'create',
        component: PostCreateComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path: 'landing',
    component: LandingComponent
  },
  {
    path: 'user',
    children: [
      {
        path: 'list',
        component: UserListComponent,
        canActivate: [AuthGuard, AdminGuard]
      },
      {
        path: 'login',
        component: LoginComponent
      }, {
        path: 'logout',
        component: UserLogoutComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'create',
        component: CreateUserComponent,
        canActivate: [AuthGuard, AdminGuard]
      }
    ]
  },
  {
    path: 'statistic',
    component: StatisticComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'report',
    component: ReportComponent,
    canActivate: [AuthGuard, TeacherGuard]
  }, {
    path: 'comment/:id',
    component: CommentComponent,
    canActivate: [AuthGuard]
  }, {
    path: '**',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

export const appRouterModule = RouterModule.forRoot(routes);
