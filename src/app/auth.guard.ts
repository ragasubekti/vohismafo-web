import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {UsersService} from './services/users.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private userService: UsersService, private router: Router) {

    }

    canActivate(next: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (!localStorage.getItem('token')) {
            this.router.navigate(['/landing']);
            return false;
        } else {
            return true;
        }
    }
}
