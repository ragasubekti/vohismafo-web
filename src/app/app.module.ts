import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';

import {HttpClientModule} from '@angular/common/http';
import {appRouterModule} from './app.routes';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {OwlModule} from 'ngx-owl-carousel';
import {TimeAgoPipe} from 'time-ago-pipe'
/** material **/
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MdAutocompleteModule,
  MdButtonModule,
  MdCardModule,
  MdCheckboxModule,
  MdChipsModule,
  MdDialogModule,
  MdIconModule,
  MdInputModule,
  MdListModule,
  MdMenuModule,
  MdProgressBarModule,
  MdProgressSpinnerModule, MdRadioModule,
  MdSelectModule,
  MdSidenavModule,
  MdSnackBarModule,
  MdTableModule,
  MdToolbarModule,
  MdDatepickerModule

} from '@angular/material';
import {CdkTableModule} from '@angular/cdk';

import {ConfirmationService, ConfirmDialogModule, DataTableModule, SharedModule} from 'primeng/primeng';
import {ChartModule} from 'angular-highcharts';

import {UsersService} from './services/users.service';
import {AuthGuard} from './auth.guard';
import {NotifyService} from './services/notify.service';
import {PostService} from './services/post.service';

import {LoginComponent} from './component/user/login/login.component';
import {HomeComponent} from './component/home/home.component';
import {UserLogoutComponent} from './component/user/logout/user-logout.component';
import {DeletePostModal} from './modal/post/delete/delete.post.modal';
import {StatusPostModal} from './modal/post/status/status.post.modal';
import {UserListComponent} from './component/user/list/list.component';
import {UserCreateModal} from 'app/modal/user/create/user.create.modal';
import {PostCreateComponent} from './component/post/create/post.create.component';
import {CreateUserComponent} from './component/user/create/create.user.component';
import {PostCancelModal} from './modal/post/cancel/post.cancel.modal';
import {SlickSliderComponent} from './directive/slick/slick';
import {UserActivationModal} from './modal/user/activation/user.activation.modal';
import {StatisticComponent} from './component/statistic/statistic.component';
import {ReportComponent} from './component/report/report.component';
import {ActivationService} from './services/activation.service';
import {CommentComponent} from './component/comment/comment.component';
import {RespondModal} from './modal/post/respond/respond.modal';
import {ResponseService} from './services/response.service';
import {ResponseModal} from './modal/post/response/response.modal';
import {AdminGuard} from './admin.guard';
import {StatisticService} from './services/statistic.service';
import {UserDeleteModal} from './modal/user/delete/user.delete.modal';
import {TeacherGuard} from './teacher.guard';
import {CommentService} from './services/comment.service';
import {CommentDeleteModal} from './modal/comment/delete/comment.delete.modal';
import {UserUpdateModal} from './modal/user/update/user.update.modal';
import {LandingComponent} from './landing/landing.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    UserLogoutComponent,
    DeletePostModal,
    StatusPostModal,
    UserListComponent,
    UserCreateModal,
    PostCreateComponent,
    CreateUserComponent,
    PostCancelModal,
    SlickSliderComponent,
    UserActivationModal,
    StatisticComponent,
    ReportComponent,
    CommentComponent,
    RespondModal,
    ResponseModal,
    UserDeleteModal,
    CommentDeleteModal,
    UserUpdateModal,
    TimeAgoPipe,
    LandingComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    appRouterModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MdCardModule,
    MdChipsModule,
    MdToolbarModule,
    MdButtonModule,
    MdMenuModule,
    MdIconModule,
    MdSidenavModule,
    MdListModule,
    MdInputModule,
    MdCheckboxModule,
    MdProgressSpinnerModule,
    MdProgressBarModule,
    MdTableModule,
    CdkTableModule,
    DataTableModule,
    SharedModule,
    MdDialogModule,
    ConfirmDialogModule,
    OwlModule,
    ReactiveFormsModule,
    MdSnackBarModule,
    MdAutocompleteModule,
    MdSelectModule,
    ChartModule,
    MdRadioModule,
    MdDatepickerModule
  ],
  providers: [
    UsersService,
    AuthGuard,
    NotifyService,
    PostService,
    ConfirmationService,
    ActivationService,
    ResponseService,
    AdminGuard,
    StatisticService,
    TeacherGuard,
    CommentService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    DeletePostModal,
    StatusPostModal,
    UserCreateModal,
    PostCancelModal,
    UserActivationModal,
    RespondModal,
    ResponseModal,
    UserDeleteModal,
    CommentDeleteModal,
    UserUpdateModal
  ]
})
export class AppModule {
}
