import {Component, OnInit} from '@angular/core';
import {UsersService} from './services/users.service';
import {AuthGuard} from './auth.guard';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  // isLoggedIn: boolean;
  buildVersion = '0.6';
  buildNumber = '495';

  constructor(private userService: UsersService, private authGuard: AuthGuard) {

  }

  storage(key) {
    return localStorage.getItem(key);
  }

  get isLoggedIn() {
    return this.userService.isLoggedIn;
  }

  get userLevel() {
    if (this.isLoggedIn) {
      return parseInt(localStorage.getItem('level'));
    } else {
      return false;
    }
  }

  ngOnInit() {
  }

}
