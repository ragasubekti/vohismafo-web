import { VohismafoPage } from './app.po';

describe('vohismafo App', () => {
  let page: VohismafoPage;

  beforeEach(() => {
    page = new VohismafoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
